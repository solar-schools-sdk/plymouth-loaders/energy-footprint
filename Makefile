.PHONY: install uninstall set-default

uninstall:
	rm -rv /usr/share/plymouth/themes/energy-footprint || true

install: uninstall
	mkdir /usr/share/plymouth/themes/energy-footprint
	cp -v *.script *.plymouth *.png /usr/share/plymouth/themes/energy-footprint/

set-default:
	plymouth-set-default-theme -R energy-footprint
