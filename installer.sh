#!/bin/sh
wget https://gitlab.com/plymouth-loaders/energy-footprint/-/archive/master/energy-footprint-master.tar.gz
tar xf energy-footprint-master.tar.gz
cd energy-footprint-master
sudo make install
cd ..
rm -rf energy-footprint-master* 
sudo plymouth-set-default-theme -R energy-footprint