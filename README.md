### Installation

    wget https://gitlab.com/solarschools/plymouth-loaders/energy-footprint/-/archive/master/energy-footprint-master.tar.gz
    tar xf energy-footprint-master.tar.gz
    cd energy-footprint-master
    sudo make install   # as root or with sudo
    cd ..
    rm -rf energy-footprint-master* 
    sudo plymouth-set-default-theme energy-footprint
    # reboot to see the new theme

### License

This theme is licensed under GPLv2, for more details check COPYING.
